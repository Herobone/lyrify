/**
 * Copyright (C) 2020 Herobone & Aynril
 *
 * This file is part of Spot The Verse.
 *
 * Spot The Verse is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Spot The Verse is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Spot The Verse.  If not, see <http://www.gnu.org/licenses/>.
 */

/* eslint-disable max-len */
export const authEndpoint = "https://accounts.spotify.com/authorize",
    clientId = "YOUR_ID_HERE",
    redirectUri = "http://localhost:3000/",
    scopes = [
        "user-top-read",
        "user-read-currently-playing",
        "user-read-playback-state",
        "playlist-read-collaborative",
        "playlist-read-private",
        "user-library-read"
    ].join("%20"),
    updateInterval = 300,
    url = `${authEndpoint}?client_id=${clientId}&redirect_uri=${redirectUri}&scope=${scopes}&response_type=token&show_dialog=false`;

/* eslint-disable max-statements */
/**
 * Copyright (C) 2020 Herobone & Aynril
 *
 * This file is part of Spot The Verse.
 *
 * Spot The Verse is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Spot The Verse is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Spot The Verse.  If not, see <http://www.gnu.org/licenses/>.
 */

/* eslint-disable max-params */
import * as Query from "./Query";
import md5 from "md5";
export const getPlaylistHashStored = () => md5(localStorage.getItem("songs")),
    getPlaylistSongs = (playlistId, callback) => Query.apiV2Query( // eslint-disable-line max-len
        `get_songs?id=${playlistId}`, // eslint-disable-line max-len
        (data) => {

            const songs = [],

                temp = data;
            for (let j = 0; j < Object.keys(temp).length; j += 1) {

                songs.push(temp[j]);

            }

            return callback(songs);

        },
        (xhr) => {

            if (xhr.status === 401) {

                Query.deleteToken();
                console.error("Unexpected error! Reload page!");

            }

        }
    ),
    loadPlaylistInfo = () => JSON.parse(localStorage.getItem("playlistInfo")),
    loadTracks = () => JSON.parse(localStorage.getItem("songs")),
    // eslint-disable-next-line max-lines-per-function
    processInfo = (playlistId) => {

        Query.apiV2Query(
            `analyse?id=${playlistId}`,
            (data) => {

                localStorage.setItem(
                    "playlistInfo",
                    JSON.stringify(data)
                );

            }
        );

    };

/**
 * Copyright (C) 2020 Herobone & Aynril
 *
 * This file is part of Spot The Verse.
 *
 * Spot The Verse is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Spot The Verse is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Spot The Verse.  If not, see <http://www.gnu.org/licenses/>.
 */

import * as Query from "./Query";
const options = {
    "difficulty": 3,
    "playing": false,
    "time": 30
};

// eslint-disable-next-line one-var
export const getOptions = () => Query.getCookie("options"),
    init = () => {

        const current = getOptions();

        if (!current || current === "undefined") {

            Query.setCookie(
                "options",
                options
            );

        }

    },
    setOptions = (newOptions) => {

        const original = getOptions();
        Query.setCookie(
            "options",
            Object.assign(
                original,
                newOptions
            )
        );

    };

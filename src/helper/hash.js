/**
 * Copyright (C) 2020 Herobone & Aynril
 *
 * This file is part of Spot The Verse.
 *
 * Spot The Verse is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Spot The Verse is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Spot The Verse.  If not, see <http://www.gnu.org/licenses/>.
 */

// Get the hash of the url
const hash = window.location.hash
    .substring(1)
    .split("&")
    .reduce(
        (initial, item) => {

            if (item) {

                const parts = item.split("=");
                initial[parts[0]] = decodeURIComponent(parts[1]);

            }
            return initial;

        },
        {}
    );
window.location.hash = "";

export default hash;

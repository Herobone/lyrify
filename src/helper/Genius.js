/**
 * Copyright (C) 2020 Herobone & Aynril
 *
 * This file is part of Spot The Verse.
 *
 * Spot The Verse is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Spot The Verse is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Spot The Verse.  If not, see <http://www.gnu.org/licenses/>.
 */

import * as Query from "./Query";

export const
    getLyricsFromSong = (track, artist, callback) => {

        Query.apiV2Query(
            `get_lyrics?track=${encodeURIComponent(track)}&artist=${encodeURIComponent(artist)}`,
            callback,
            (xhr, status, error) => {

                if (status === 404) {

                    console.warn("Song has no lyrics");

                }
                return callback(null);

            }
        );

    };

/* eslint-disable sort-vars */
/* eslint-disable max-lines-per-function */
/**
 * Copyright (C) 2020 Herobone & Aynril
 *
 * This file is part of Spot The Verse.
 *
 * Spot The Verse is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Spot The Verse is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Spot The Verse.  If not, see <http://www.gnu.org/licenses/>.
 */

import Cookies from "universal-cookie";
import jquery from "jquery";

const cookies = new Cookies();

let lock = false;

// eslint-disable-next-line one-var
export const addChangeHandler = (callback) => cookies.addChangeListener(callback),
    apiQuery = (url, callback, errorHandler) => {

        const token = getToken();

        if (token === "Anonymous") {

            return;

        }

        jquery.ajax({
            "beforeSend": (xhr) => {

                xhr.setRequestHeader(
                    "Authorization",
                    `Bearer ${token}`
                );

            },
            "error": (xhr, status, error) => {

                if (errorHandler) {

                    errorHandler(
                        xhr,
                        status,
                        error
                    );

                } else {

                    console.error("There was an error");
                    const errorMessage = `${xhr.status}: ${xhr.statusText}`;
                    console.error(errorMessage);

                }

            },
            "success": (data) => {

                callback(data);

            },

            "type": "GET",
            "url": `https://api.spotify.com/v1/${url}`
        });

    },
    apiV2Query = (url, callback, errorHandler) => {

        jquery.ajax({
            "beforeSend": (xhr) => {

                xhr.setRequestHeader(
                    "Token",
                    getToken()
                );

            },
            "error": (xhr, status, error) => {

                if (errorHandler) {

                    errorHandler(
                        xhr,
                        status,
                        error
                    );

                } else {

                    console.error("There was an error");
                    const errorMessage = `${xhr.status}: ${xhr.statusText}`;
                    console.error(errorMessage);

                }

            },
            "success": (data) => {

                callback(data);

            },

            "type": "GET",
            "url": `https://europe-west1-spot-the-verse-backend.cloudfunctions.net/${url}`
            // "url": `http://localhost:8080/${url}`
        });

    },
    deleteCookie = (name) => cookies.remove(name),
    deleteToken = () => deleteCookie("token"),
    getCookie = (name) => cookies.get(name),
    getSelectedPlaylist = () => getCookie("selectedPlaylist"),
    // eslint-disable-next-line max-statements
    getTokenFromCode = (onLoaded) => {

        if (lock) {

            return;

        }

        let token = cookies.get("token"),
            expiresAt = null;

        if (!token || token === "undefined" || typeof token === "undefined") {

            // Const code = findGetParameter("code");
            const params = new URLSearchParams(window.location.search),
                code = params.get("code");
            if (code !== "" &&
                code !== null &&
                code !== "null" &&
                typeof code !== "undefined") {

                lock = true;

                apiV2Query(
                    `auth?code=${code}`,
                    (data) => {

                        // eslint-disable-next-line prefer-destructuring
                        token = data.token;
                        expiresAt = new Date().getTime() + (data.expires - 100) * 1000;
                        setCookies(
                            token,
                            expiresAt
                        );

                        localStorage.setItem(
                            "refreshToken",
                            data.refresh
                        );

                        lock = false;
                        if (onLoaded !== null) {

                            onLoaded();

                        }

                    }
                );

            } else {

                refreshUserToken(onLoaded);

            }

        }

    },
    getIsAnonymous = () => {

        const token = getToken();
        return token === "Anonymous" || token === null;

    },
    getToken = () => {

        if (lock) {

            return null;

        }
        const token = cookies.get("token"),
            time = new Date();
        if (cookies.get("expires") < time.getTime()) {

            refreshUserToken();

        }
        return token;

    },
    refreshUserToken = (onLoaded) => {

        lock = true;
        const refreshToken = localStorage.getItem("refreshToken");
        if (refreshToken && refreshToken !== "" && typeof refreshToken !== "undefined") {

            apiV2Query(
                `auth?refresh=${refreshToken}`,
                (data) => {

                    const {token} = data,
                        expiresAt = new Date().getTime() + (data.expires - 100) * 1000;
                    setCookies(
                        token,
                        expiresAt
                    );
                    lock = false;
                    if (onLoaded !== null) {

                        onLoaded();

                    }

                }
            );

        }

    },
    removeChangeHandler = (callback) => cookies.removeChangeListener(callback),
    setCookie = (name, content) => {

        cookies.set(
            name,
            content,
            {
                "expires": new Date(Date.now() + 2592000)
            }
        );

    },
    setCookies = (token, expiresAt) => {

        // Console.info(`Saving token ${token} until ${new Date(expiresAt)}`);

        cookies.set(
            "token",
            token,
            {
                "expires": new Date(expiresAt),
                "path": "/"
            }
        );

        cookies.set(
            "expires",
            expiresAt,
            {
                "expires": new Date(expiresAt),
                "path": "/"
            }
        );

    };

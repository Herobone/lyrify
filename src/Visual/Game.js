/**
 * Copyright (C) 2020 Herobone & Aynril
 *
 * This file is part of Spot The Verse.
 *
 * Spot The Verse is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Spot The Verse is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Spot The Verse.  If not, see <http://www.gnu.org/licenses/>.
 */

/* eslint-disable max-statements */
import "../css/Game.css";
import * as Genius from "../helper/Genius";
import * as Options from "../helper/Options";
import * as PropTypes from "prop-types";
import React, {PureComponent} from "react";
import Answer from "./Answer";
import TimeBar from "./TimeBar";

class Game extends PureComponent {

    static propTypes = {
        // eslint-disable-next-line react/no-unused-prop-types
        "rerenderParentCallback": PropTypes.func
    }

    static defaultProps = {
        "rerenderParentCallback": () => {

            console.warn("No rerender was given! Refresh manually");

        }
    }

    // eslint-disable-next-line max-statements
    constructor () {

        super();
        this.state = {
            "failed": false,
            "line": null,
            "loading": "Something",
            "selectedTrack": null
        };
        this.rerenderParentCallback = this.rerenderParentCallback.bind(this);
        this.setParentStateCallback = this.setParentStateCallback.bind(this);
        this.resetTimer = this.resetTimer.bind(this);
        this.startTimer = this.startTimer.bind(this);
        this.stopTimer = this.stopTimer.bind(this);
        this.handleItemClick = this.handleItemClick.bind(this);
        this.handleGameButton = this.handleGameButton.bind(this);
        this.timer = React.createRef();

    }

    componentDidMount () {

        this.onMounted();

    }

    componentDidUpdate (_prevProps, _prevState) {

        const {failed} = this.state;
        if (failed) {

            this.stopTimer();

        }

    }

    componentWillUnmount () {

        Options.setOptions({"playing": false});

    }

    rerenderParentCallback () {

        this.forceUpdate();

    }

    setParentStateCallback (incomingState) {

        this.setState(incomingState);

    }

    onMounted () {

        this.getSolution();

    }

    resetTimer () {

        this.timer.current.reset();

    }

    startTimer () {

        this.timer.current.start();

    }

    stopTimer () {

        this.timer.current.stop();

    }

    renderLoading (what) {

        if (this.timer.current !== null) {

            this.timer.current.show(what === null);

        }

        let returnIt = "";
        if (what !== null) {

            returnIt =
                (<div className="loading">
                    <i className="fas fa-sync-alt w3-spin fa-9x w3-padding-64" />
                    <br />
                    {`Loading ${what}. Please be patient`}
                 </div>);

        }

        return returnIt;

    }

    random (mn, mx) {

        return Math.random() * (mx - mn) + mn;

    }

    selectRandom (arr) {

        return arr[Math.floor(this.random(
            1,
            arr.length
        )) - 1];

    }

    /**
     * Shuffles array in place.
     * @param {Array} array items An array containing the items.
     */
    shuffle (array) {

        for (let i = array.length - 1; i > 0; i -= 1) {

            const j = Math.floor(Math.random() * (i + 1));
            [
                array[i],
                array[j]
            ] = [
                array[j],
                array[i]
            ];

        }
        return array;

    }

    handleItemClick (track) {

        const {selectedTrack} = this.state;
        this.stopTimer();

        // eslint-disable-next-line no-negated-condition
        if (track.name !== selectedTrack.name) {

            this.setState({
                "failed": true,
                "whyFailed": "picked the wrong Track"
            });

        } else {

            this.getSolution();

        }


    }

    getXAnswers () {

        const control = [],
            toReturn = [],
            tracks = JSON.parse(localStorage.getItem("songs")),
            {selectedTrack} = this.state;

        // eslint-disable-next-line function-paren-newline
        toReturn.push(
            <Answer
                key={selectedTrack.name}
                onRowClick={this.handleItemClick}
                track={selectedTrack}
            />);
        control.push(selectedTrack.name);

        for (let i = 0; i < Options.getOptions().difficulty - 1; i += 1) {

            let goOn = true;
            while (goOn) {

                const track = this.selectRandom(tracks);
                if (!control.includes(track.name)) {

                    goOn = false;

                    control.push(track.name);

                    // eslint-disable-next-line function-paren-newline
                    toReturn.push(
                        <Answer
                            key={track.name}
                            onRowClick={this.handleItemClick}
                            track={track}
                        />);

                }

            }

        }

        this.shuffle(toReturn);
        this.resetTimer();
        this.startTimer();

        return toReturn;

    }

    getSolution () {

        this.setState({"loading": "Next Track"});

        const tracks = JSON.parse(localStorage.getItem("songs")),
            // eslint-disable-next-line sort-vars
            track = this.selectRandom(tracks);
        this.setState({"selectedTrack": track});

        this.getLyricLine(track);

    }

    mightSpoiler (line, track) {

        // Those words are allowed 'cause they don't spoiler anything
        const allowedWords = [
                "and",
                "this",
                "yeah",
                "no",
                "yes",
                "you",
                "me",
                "he",
                "she",
                "it"
            ],
            // Convert the track  name to lowe case to get a consistent compare
            name = track.toLowerCase(),

            /*
             * Replace symbols that might cause non-flags, convert to lower to be consistent
             * and then split the line at a space to compare each word individually
             */
            splitted = line
                .replace(
                    // eslint-disable-next-line prefer-named-capture-group
                    /(,\.-\/\(\)!\?)/gmu,
                    ""
                )
                .toLowerCase()
                .split(" ");

        let spoiler = false;

        // eslint-disable-next-line consistent-return
        splitted.forEach((split) => {

            let temporarySpoiler = false;

            // If the current word is contained in the title
            if (name.includes(split) || split.includes(name)) {

                temporarySpoiler = true;

            }

            // If the current word is a allowed word, don't flag the line for that
            if (allowedWords.includes(split)) {

                temporarySpoiler = false;

            }

            // Combine global and local spoiler
            spoiler = spoiler || temporarySpoiler;

        });

        return spoiler;

    }

    getLyricLine (selectedTrack) {

        Genius.getLyricsFromSong(
            selectedTrack.name,
            selectedTrack.artists[0].name,
            (lyric) => {

                if (lyric === null ||
                    typeof lyric === "undefined") {

                    return this.getSolution();

                }

                let line = "",
                    tries = 0,
                    unfinished = true;

                while (unfinished) {

                    line = this.selectRandom(lyric);

                    if (line === null ||
                    typeof line === "undefined") {

                        tries += 1;
                        line = this.selectRandom(lyric);

                    } else if (line.length < 6 ||
                    this.mightSpoiler(
                        line,
                        selectedTrack.name
                    )) {

                        tries += 1;
                        line = this.selectRandom(lyric);

                    } else {

                        unfinished = false;
                        break;

                    }

                    if (tries >= 10) {

                        return this.getSolution();

                    }

                }

                this.setState({
                    line
                });
                this.setState({"loading": null});
                return this.forceUpdate();

            }
        );

    }

    getState () {

        return this.state;

    }

    handleGameButton () {

        Options.setOptions({"playing": false});

    }

    // eslint-disable-next-line max-lines-per-function
    render () {

        const {line, loading, failed, selectedTrack, whyFailed} = this.state;

        return (
            <div className="game">
                {!loading && !failed &&
                <div className="lyrics">
                    {line}
                </div>}
                <TimeBar
                    ref={this.timer}
                    setParentStateCallback={this.setParentStateCallback}
                    tick={this.timerTick}
                />
                {!loading && !failed &&
                    <div>
                        <table className="game-wrapper">
                            <tbody>
                                {this.getXAnswers()}
                            </tbody>
                        </table>
                    </div>}
                {failed &&
                <div>
                    {`You ${whyFailed}. It was ${selectedTrack.name}`}
                    <br />
                    <button
                        className="w3-button w3-round w3-green"
                        onClick={this.handleGameButton}
                        type="button"
                    >
                        Go Back
                    </button>
                </div>}
                {this.renderLoading(loading)}
            </div>);

    }

}

export default Game;

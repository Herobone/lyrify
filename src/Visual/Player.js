/**
 * Copyright (C) 2020 Herobone & Aynril
 *
 * This file is part of Spot The Verse.
 *
 * Spot The Verse is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Spot The Verse is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Spot The Verse.  If not, see <http://www.gnu.org/licenses/>.
 */

/* eslint-disable max-lines-per-function */
/* eslint-disable no-ternary */
/* eslint no-console: ["warn", { allow: ["warn", "error"] }] */
import "../css/Player.css";
import * as PropTypes from "prop-types";
import * as Query from "../helper/Query";
import * as config from "../config/config";
import React, {PureComponent} from "react";

class Player extends PureComponent {

    static propTypes = {
        "rerenderParentCallback": PropTypes.func
    }

    static defaultProps = {
        "rerenderParentCallback": () => {

            console.warn("No rerender was given! Refresh manually");

        }
    }

    constructor () {

        super();
        this.state = this.getStandardStates();

    }

    componentDidMount () {

        this.onMounted();

    }

    componentWillUnmount () {

        const {intervalId, intervalId2} = this.state;
        clearInterval(intervalId);
        clearInterval(intervalId2);

    }

    // eslint-disable-next-line class-methods-use-this
    getStandardStates () {

        return {
            "isPlaying": "Paused",
            "item": {
                "album": {
                    "images": [{"url": ""}]
                },
                "artists": [
                    {
                        "name": ""
                    }
                ],
                "duration_ms": 0,
                "name": "notloaded"
            },
            "progressMs": 0
        };

    }

    onMounted () {

        this.getPlaying();

        const interval = setInterval(
                this.getPlaying.bind(this),
                3000
            ),
            interval2 = setInterval(
                this.updateFluentProgressBar.bind(this),
                config.updateInterval
            );
        this.setState({
            "intervalId": interval,
            "intervalId2": interval2
        });

    }

    getPlaying () {

        const {rerenderParentCallback} = this.props;

        Query.apiQuery(
            "me/player",
            (data) => {

                try {

                    this.setState({
                        "isPlaying": data.is_playing,
                        "item": data.item,
                        "progressMs": data.progress_ms
                    });

                } catch {

                    this.setState(this.getStandardStates());

                }

            },
            (xhr) => {

                if (xhr.status === 401) {

                    Query.deleteToken();
                    this.setState(this.getStandardStates());
                    rerenderParentCallback();

                }

            }
        );

    }

    updateFluentProgressBar () {

        const {item, progressMs} = this.state;
        if (item.name === "notloaded") {

            return;

        }

        if (progressMs + 500 < item.duration_ms) {

            this.setState({
                "progressMs": progressMs + config.updateInterval
            });

        } else {

            this.getPlaying();

        }

    }

    render () {

        let returnThat = "";

        const {item, progressMs, isPlaying} = this.state,

            progressBarStyles = {
                "width": `${progressMs * 100 / item.duration_ms}%`
            };

        if (this.state !== this.getStandardStates() &&
            item.name !== "notloaded" &&
            item &&
            progressMs &&
            isPlaying) {

            // eslint-disable-next-line no-extra-parens
            returnThat = (
                <div
                    className="Player
                            w3-bar-item
                            w3-right
                            w3-hide-medium
                            w3-hide-small"
                >
                    {
                        item && progressMs && isPlaying &&
                        <div className="player-wrapper">
                            <div className="now-playing__img">
                                <img
                                    alt="Song cover"
                                    src={item.album.images[0].url}
                                />
                            </div>
                            <div className="now-playing__side">
                                <div className="now-playing__name">
                                    {item.name}
                                </div>
                                <div className="now-playing__artist">
                                    {item.artists[0].name}
                                </div>
                            </div>
                            <div className="progress">
                                <div
                                    className="progress__bar"
                                    style={progressBarStyles}
                                />
                            </div>
                        </div>
                    }
                </div>);

        } else {

            // eslint-disable-next-line no-extra-parens
            returnThat = (
                <div
                    className="Player
                            w3-bar-item
                            w3-right
                            w3-hide-medium
                            w3-hide-small
                            now-playing__name"
                >
                    Currently Playing Nothing
                </div>);

        }

        return returnThat;

    }

}

export default Player;

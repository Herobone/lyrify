/**
 * Copyright (C) 2020 Herobone & Aynril
 *
 * This file is part of Spot The Verse.
 *
 * Spot The Verse is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Spot The Verse is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Spot The Verse.  If not, see <http://www.gnu.org/licenses/>.
 */

/* eslint-disable no-invalid-this */
import "../css/Game.css";
import * as PropTypes from "prop-types";
import React, {PureComponent} from "react";

class Answer extends PureComponent {

    static propTypes = {
        "onRowClick": PropTypes.func.isRequired,
        "track": PropTypes.shape({
            "album": PropTypes.shape({
                "images": PropTypes.shape({
                    "normalImage": PropTypes.shape({
                        "url": PropTypes.string.isRequired
                    })
                }).isRequired,
                "name": PropTypes.string.isRequired
            }).isRequired,
            "artists": PropTypes.arrayOf(PropTypes.shape({
                "name": PropTypes.string.isRequired
            })).isRequired,
            "name": PropTypes.string.isRequired
        }).isRequired
    }

    constructor () {

        super();
        this.handleClick = this.handleClick.bind(this);

    }

    handleClick = () => {

        const {onRowClick, track} = this.props;
        onRowClick(track);

    }

    render () {

        const {track} = this.props;
        let artists = "";

        for (let i = 0; i < track.artists.length; i += 1) {

            const element = track.artists[i].name;
            if (i !== 0) {

                artists += ", ";

            }

            artists += element;

        }
        return (
            <tr
                className="track-answer w3-padding-16"
                onClick={this.handleClick}
            >
                <td className="track-img">
                    <img
                        alt={track.artists[0].name}
                        className="track-img"
                        src={track.album.images.normalImage.url}
                    />
                </td>
                <td className="track-side">
                    <div className="track-name">
                        {track.name}
                    </div>
                    <div className="track-artist">
                        {`by ${artists}`}
                    </div>
                </td>
            </tr>
        );

    }

}

export default Answer;

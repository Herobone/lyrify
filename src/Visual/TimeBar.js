/**
 * Copyright (C) 2020 Herobone & Aynril
 *
 * This file is part of Spot The Verse.
 *
 * Spot The Verse is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Spot The Verse is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Spot The Verse.  If not, see <http://www.gnu.org/licenses/>.
 */

import * as Config from "../config/config.js";
import * as Options from "../helper/Options";
import * as PropTypes from "prop-types";
import React, {PureComponent} from "react";

class TimeBar extends PureComponent {

    static propTypes = {
        "setParentStateCallback": PropTypes.func,
        "tick": PropTypes.func
    }

    static defaultProps = {
        "setParentStateCallback": (states) => {

            console.warn("No parent State Callback was given!");

        },
        "tick": () => {
            // Do nothing if nothing was extra defined
        }
    }

    constructor () {

        super();
        this.state = {
            "progressMs": 0,
            "running": false,
            "shouldShow": true,
            "time": 30 * 1000
        };

    }

    componentDidMount () {

        this.onMount();

    }

    componentWillUnmount () {

        const {intervalId} = this.state;
        clearInterval(intervalId);

    }

    onMount () {

        Options.init();

        const interval = setInterval(
            this.updateFluentProgressBar.bind(this),
            Config.refreshRate
        );

        this.setState({
            "intervalId": interval,
            "time": Options.getOptions().time * 1000
        });


    }

    // Reset the timer
    reset () {

        this.setState({
            "progressMs": 0
        });

    }

    // Start the timer
    start () {

        this.setState({
            "running": true
        });

    }

    // Stop (Pause) the timer
    stop () {

        this.setState({
            "running": false
        });

    }

    show (shouldShow) {

        this.setState({
            shouldShow
        });

    }

    updateFluentProgressBar () {

        const {time, progressMs, running} = this.state,
            {setParentStateCallback, tick} = this.props;

        tick();
        if (!running) {

            return;

        }

        if (progressMs + Config.refreshRate < time) {

            this.setState({
                "progressMs": progressMs + Config.refreshRate
            });

        } else {

            setParentStateCallback({
                "failed": true,
                "whyFailed": "timed out"
            });

        }

    }


    render () {

        const {time, progressMs, shouldShow} = this.state,

            progressBarStyles = {
                "height": "3px",
                "width": `${100 - progressMs * 100 / time}%`
            };

        return (
            <div
                className="w3-dark-grey timebar"
                style={{"visibility": shouldShow
                    ? "visible"
                    : "hidden"}}
            >
                <div
                    className="w3-yellow"
                    style={progressBarStyles}
                />
            </div>
        );

    }

}

export default TimeBar;

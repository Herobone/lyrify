/*
 * Copyright (C) 2020 Herobone & Aynril
 *
 * This file is part of Spot The Verse.
 *
 * Spot The Verse is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Spot The Verse is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Spot The Verse.  If not, see <http://www.gnu.org/licenses/>.
 */

/* eslint-disable no-invalid-this */
import "../css/App.css";
import * as PropTypes from "prop-types";
import React, {PureComponent} from "react";
class PlaylistElement extends PureComponent {

    static propTypes = {
        "entry": PropTypes.shape({
            "images": PropTypes.arrayOf(PropTypes.shape({
                "url": PropTypes.string.isRequired
            }).isRequired),
            "name": PropTypes.string.isRequired,
            "tracks": PropTypes.shape({
                "total": PropTypes.number.isRequired
            }).isRequired
        }).isRequired,
        "onRowClick": PropTypes.func.isRequired
    }

    constructor () {

        super();
        this.handleClick = this.handleClick.bind(this);

    }

    handleClick = () => {

        const {onRowClick, entry} = this.props;
        onRowClick(entry);

    }

    // eslint-disable-next-line max-statements
    render () {

        const {entry} = this.props;
        let image = "",
            totalTracks = 0,
            trackName = "Failed";

        if (entry) {

            if (entry.images) {

                const {images} = entry,
                    [firstIndex] = images;
                if (firstIndex) {

                    image = firstIndex.url;

                }

            }

            if (entry.name) {

                const {name} = entry;
                trackName = name;

            }

            if (entry.tracks) {

                const {tracks} = entry,
                    {total} = tracks;
                totalTracks = total;

            }

        }
        return (
            <tr
                className="playlist-entry w3-padding-16"
                onClick={this.handleClick}
            >
                <td className="playlist-img">
                    <img
                        alt="Playlist Cover"
                        className="playlist-img"
                        height="100px"
                        src={`${image}`}
                        width="100px"
                    />
                </td>
                <td className="playlist-side">
                    <div className="playlist-name">
                        {trackName}
                    </div>
                    <div className="playlist-length">
                        Tracks:
                        {" "}
                        {totalTracks}
                    </div>
                </td>
            </tr>
        );

    }

}

export default PlaylistElement;

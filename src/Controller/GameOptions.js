/* eslint-disable no-negated-condition */
/**
 * Copyright (C) 2020 Herobone & Aynril
 *
 * This file is part of Spot The Verse.
 *
 * Spot The Verse is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Spot The Verse is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Spot The Verse.  If not, see <http://www.gnu.org/licenses/>.
 */

/* eslint-disable react/jsx-max-depth */
/* eslint-disable max-lines-per-function */
import "../css/App.css";
import * as Options from "../helper/Options";
import * as Playlist from "../helper/Playlist";
import * as PropTypes from "prop-types";
import * as Query from "../helper/Query";
import React, {PureComponent} from "react";

class GameOptions extends PureComponent {

    static propTypes = {
        "rerenderAppCall": PropTypes.func
    }

    static defaultProps = {
        "rerenderAppCall": () => {

            console.warn("No rerender was given! Refresh manually");

        }
    }


    constructor () {

        super();
        this.state = {
            "difficulty": 3,
            "isReady": false,
            "time": 30
        };
        this.handleOptionChange = this.handleOptionChange.bind(this);
        this.getDifficultySelector = this.getDifficultySelector.bind(this);
        this.getTimeSelector = this.getTimeSelector.bind(this);
        this.handleButtonClick = this.handleButtonClick.bind(this);

    }

    componentDidMount () {

        this.onMount();

    }

    // eslint-disable-next-line max-statements
    onMount () {

        Options.init();
        this.setState({
            "difficulty": Options.getOptions().difficulty,
            "time": Options.getOptions().time
        });

        const {tracks} = Query.getSelectedPlaylist();

        if (!localStorage.getItem("songs") ||
            localStorage.getItem("songs") === null ||
            typeof localStorage.getItem("songs") === "undefined") {

            this.getSongs();

        } else if (Playlist.loadTracks().length !== tracks.total) {

            this.getSongs();

        } else {

            this.setState({"isReady": true});

        }

    }

    getSongs () {

        const {id} = Query.getSelectedPlaylist();

        Playlist.getPlaylistSongs(
            id,
            (songsIn) => {

                localStorage.setItem(
                    "songs",
                    JSON.stringify(songsIn)
                );
                this.setState({"isReady": true});

            }
        );
        Playlist.processInfo(id);

    }

    handleButtonClick (event) {

        const caller = event.target.name,
            {isReady} = this.state,
            {rerenderAppCall} = this.props;

        if (caller === "play-button") {

            if (isReady) {

                if (Playlist.loadPlaylistInfo().speech > 0.5) {

                    alert("Most likely an audiobook! Returning!");
                    Query.deleteCookie("selectedPlaylist");
                    rerenderAppCall();


                } else {

                    Options.setOptions({"playing": true});

                }

            } else {

                alert("Not ready jet");

            }

        }

    }

    handleOptionChange (event) {

        const caller = event.target.name,
            value = parseInt(
                event.target.value,
                10
            );

        Options.setOptions({
            [caller]: value
        });

        this.setState({
            [caller]: value
        });

    }

    getDifficultySelector () {

        const {difficulty} = this.state;

        return (
            <div className="w3-margin-left">
                <label>
                    <input
                        checked={difficulty === 3}
                        className="w3-radio"
                        name="difficulty"
                        onChange={this.handleOptionChange}
                        type="radio"
                        value="3"
                    />
                    {" "}
                    Easy        (3 to Choose)
                </label>
                <br />
                <label>
                    <input
                        checked={difficulty === 5}
                        className="w3-radio"
                        name="difficulty"
                        onChange={this.handleOptionChange}
                        type="radio"
                        value="5"
                    />
                    {" "}
                    Acceptable  (5 to Choose)
                </label>
                <br />
                <label>
                    <input
                        checked={difficulty === 7}
                        className="w3-radio"
                        name="difficulty"
                        onChange={this.handleOptionChange}
                        type="radio"
                        value="7"
                    />
                    {" "}
                    Hard        (7 to Choose)
                </label>
                <br />
                <label>
                    <input
                        checked={difficulty === -1}
                        className="w3-radio"
                        name="difficulty"
                        onChange={this.handleOptionChange}
                        type="radio"
                        value="-1"
                    />
                    {" "}
                    Hardcore    (None to Choose)
                </label>
            </div>
        );

    }

    // eslint-disable-next-line max-lines-per-function
    getTimeSelector () {

        const {time} = this.state;

        return (
            <div className="w3-margin-left">
                <label>
                    <input
                        checked={time === 30}
                        className="w3-radio"
                        name="time"
                        onChange={this.handleOptionChange}
                        type="radio"
                        value="30"
                    />
                    {" "}
                    30 Seconds
                </label>
                <br />
                <label>
                    <input
                        checked={time === 20}
                        className="w3-radio"
                        name="time"
                        onChange={this.handleOptionChange}
                        type="radio"
                        value="20"
                    />
                    {" "}
                    20 Seconds
                </label>
                <br />
                <label>
                    <input
                        checked={time === 10}
                        className="w3-radio"
                        name="time"
                        onChange={this.handleOptionChange}
                        type="radio"
                        value="10"
                    />
                    {" "}
                    10 Seconds
                </label>
                <br />
                <label>
                    <input
                        checked={time === 5}
                        className="w3-radio"
                        name="time"
                        onChange={this.handleOptionChange}
                        type="radio"
                        value="5"
                    />
                    {" "}
                    5 Seconds
                </label>
            </div>
        );

    }

    getOptions () {

        return (
            <div>
                <div className="w3-row">
                    <div
                        className="w3-large w3-half w3-left-align"
                    >
                        <h3 className="w3-margin-left">
                            <u>
                                <b>
                                    Difficulty
                                </b>
                            </u>
                        </h3>
                        {this.getDifficultySelector()}
                    </div>
                    <div
                        className="w3-large w3-half w3-left-align"
                    >
                        <h3 className="w3-margin-left">
                            <u>
                                <b>
                                    Time
                                </b>
                            </u>
                        </h3>
                        {this.getTimeSelector()}
                    </div>
                </div>
                <button
                    className="w3-button
                            w3-yellow
                            w3-circle
                            w3-round-large
                            w3-large
                            w3-padding-16
                            w3-margin-top"
                    name="play-button"
                    onClick={this.handleButtonClick}
                    type="submit"
                >
                    <i className="fas fa-play" />
                    {" "}
                    Play
                </button>
            </div>
        );

    }

    render () {

        return (
            <div className="w3-margin-top w3-padding-16">
                {this.getOptions()}
            </div>
        );

    }

}

export default GameOptions;

/**
 * Copyright (C) 2020 Herobone & Aynril
 *
 * This file is part of Spot The Verse.
 *
 * Spot The Verse is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Spot The Verse is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Spot The Verse.  If not, see <http://www.gnu.org/licenses/>.
 */

/* eslint no-console: ["warn", { allow: ["warn", "error"] }] */
import "../css/App.css";
import * as PropTypes from "prop-types";
import * as Query from "../helper/Query";
import React, {PureComponent} from "react";
import PlaylistElement from "../Visual/PlaylistElement";

class PlaylistSelector extends PureComponent {

    static propTypes = {
        "rerenderParentCallback": PropTypes.func
    }

    static defaultProps = {
        "rerenderParentCallback": () => {

            console.warn("No rerender was given! Refresh manually");

        }
    }

    constructor () {

        super();
        this.state = {
            "playlists": [
                {
                    "id": "notloaded",
                    "images": [{"url": ""}],
                    "name": "",
                    "tracks": {
                        "total": 0
                    }
                }
            ]
        };
        this.handleItemClick = this.handleItemClick.bind(this);
        this.getPlaylists = this.getPlaylists.bind(this);

    }

    componentDidMount () {

        this.onMounted();

    }

    onMounted () {

        this.getPlaylists();

    }

    getPlaylists () {

        Query.apiQuery(
            "me/playlists?limit=50",
            (data) => {

                this.setState({
                    "playlists": data.items
                });

            }
        );

    }

    handleItemClick (entry) {

        const {rerenderParentCallback} = this.props,
            content = {
                "id": "notloaded",
                "images": [{"url": ""}],
                "name": "",
                "tracks": {
                    "total": 0
                }
            };

        content.id = entry.id;
        content.images[0].url = entry.images[0].url;
        content.name = entry.name;
        content.tracks.total = entry.tracks.total;

        Query.setCookie(
            "selectedPlaylist",
            content
        );
        rerenderParentCallback();

    }

    generatePlaylistTable () {

        const content = [],
            {playlists} = this.state;

        if (playlists[0].id !== "notloaded") {

            for (let i = 0; i < playlists.length; i += 1) {

                // eslint-disable-next-line function-paren-newline
                content.push(
                    <PlaylistElement
                        entry={playlists[i]}
                        key={i.toString()}
                        onRowClick={this.handleItemClick}
                    />);

            }

        }
        return content;

    }

    render () {

        const {playlists} = this.state;
        return (
            <div className="w3-margin-top w3-padding-16">
                <div className="main-wrapper">
                    <table>
                        <tbody>
                            {
                                playlists &&
                                this.generatePlaylistTable()
                            }
                        </tbody>
                    </table>
                </div>
            </div>
        );

    }

}

export default PlaylistSelector;

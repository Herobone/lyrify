/**
 * Copyright (C) 2020 Herobone & Aynril
 *
 * This file is part of Spot The Verse.
 *
 * Spot The Verse is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Spot The Verse is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Spot The Verse.  If not, see <http://www.gnu.org/licenses/>.
 */

import "./css/index.css";
import * as serviceWorker from "./serviceWorker";
import App from "./App.js";
import {BrowserRouter} from "react-router-dom";
import React from "react";
import ReactDOM from "react-dom";

ReactDOM.render(
    <BrowserRouter>
        <App />
    </BrowserRouter>,
    document.getElementById("root")
);

/*
 * If you want your app to work offline and load faster, you can change
 * unregister() to register() below. Note this comes with some pitfalls.
 * Learn more about service workers: https://bit.ly/CRA-PWA
 */
serviceWorker.unregister();

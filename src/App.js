/**
 * Copyright (C) 2020 Herobone & Aynril
 *
 * This file is part of Spot The Verse.
 *
 * Spot The Verse is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Spot The Verse is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Spot The Verse.  If not, see <http://www.gnu.org/licenses/>.
 */

import "./css/App.css";
import "./css/style.css";
import * as Options from "./helper/Options";
import * as Query from "./helper/Query";
import * as config from "./config/config";
import React, {PureComponent} from "react";
import Game from "./Visual/Game";
import GameOptions from "./Controller/GameOptions";
import Player from "./Visual/Player";
import PlaylistSelector from "./Controller/PlaylistSelector";
import logo from "./media/Logo.png";
import {Redirect} from "react-router";

class App extends PureComponent {

    // eslint-disable-next-line max-statements
    constructor () {

        super();
        this.state = {
            "codeToTokenSucessfull": false,
            // eslint-disable-next-line react/no-unused-state
            "height": 0,
            "playing": false,
            "width": 0
        };
        this.rerenderParentCallback = this.rerenderParentCallback.bind(this);
        this.handleHeaderClick = this.handleHeaderClick.bind(this);
        this.setParentStateCallback = this.setParentStateCallback.bind(this);
        this.updateWindowDimensions = this.updateWindowDimensions.bind(this);
        this.handleCookieChange = this.handleCookieChange.bind(this);
        this.resetCodeState = this.setCodeState.bind(
            this,
            false
        );
        this.setCodeStateActive = this.setCodeState.bind(
            this,
            true
        );
        this.handleAnonymousClick = this.handleAnonymousClick.bind(this);
        this.useAnonymous = this.useAnonymous.bind(this);
        this.gameOptionRef = React.createRef();

    }

    componentDidMount () {

        this.onMounted();
        this.updateWindowDimensions();
        window.addEventListener(
            "resize",
            this.updateWindowDimensions
        );
        Query.addChangeHandler(this.handleCookieChange);

    }

    componentWillUnmount () {

        window.removeEventListener(
            "resize",
            this.updateWindowDimensions
        );
        Options.setOptions({"playing": false});
        Query.removeChangeHandler(this.handleCookieChange);

    }

    updateWindowDimensions () {

        this.setState({
            // eslint-disable-next-line react/no-unused-state
            "height": window.innerHeight,
            "width": window.innerWidth
        });

    }

    rerenderParentCallback () {

        this.forceUpdate();

    }

    setParentStateCallback (incomingState) {

        this.setState(incomingState);

    }

    onMounted () {

        Query.getTokenFromCode(() => {

            this.setCodeStateActive();

        });
        Options.init();

    }

    handleHeaderClick () {

        Query.deleteCookie("selectedPlaylist");
        this.setState({
            "playing": false
        });
        this.forceUpdate();

    }

    getHeader () {

        const selectedPlaylist = Query.getSelectedPlaylist(),
            token = Query.getToken(),
            {width, playing} = this.state;
        return (
            <div className="w3-bar w3-black w3-padding w3-card w3-margin-bottom">
                <button
                    className="w3-bar-item
                            w3-left
                            w3-button
                            logo
                            w3-circle
                            w3-round-xlarge"
                    onClick={this.handleHeaderClick}
                    type="button"
                >
                    <img
                        alt="STV"
                        className="logo"
                        src={logo}
                    />
                    {" "}
                    SpotTheVerse
                </button >
                {token && selectedPlaylist && (!playing && width < 998) &&
                    <div
                        className="w3-bar-item
                                w3-center
                                selected_playlist"
                    >
                        Selected Playlist:
                        {" "}
                        <br />
                        {selectedPlaylist.name}
                    </div>}
                {token && width > 998 && !Query.getIsAnonymous() &&
                    <Player
                        rerenderParentCallback={this.rerenderParentCallback}
                    />}
            </div>);

    }

    handleAnonymousClick () {

        this.useAnonymous();

    }

    useAnonymous () {

        console.info("Logging in as Anonymous");
        const content = {
                "id": "5EbJnUOwcDri4qwtwQbsl0",
                // eslint-disable-next-line max-len
                "images": [{"url": "https://mosaic.scdn.co/640/ab67616d0000b273298b2a8b04fae7951939294fab67616d0000b273871d85943145dde548f4ae09ab67616d0000b27398a02fef3a8b1d80a0f164ecab67616d0000b273d8c547197fdb37c104fc0df6"}],
                "name": "Anonymous Playlist",
                "tracks": {
                    "total": 217
                }
            },
            current = new Date(),
            day = current.getDate(),
            month = current.getMonth(),
            year = current.getFullYear(),
            // eslint-disable-next-line sort-vars
            in2Years = new Date(
                year + 2,
                month,
                day
            );

        Query.setCookies(
            "Anonymous",
            in2Years.getTime()
        );

        Query.setCookie(
            "selectedPlaylist",
            content
        );
        this.forceUpdate();

    }

    renderLoginButton () {

        return (
            <div>
                <a
                    className="w3-button
                            w3-green
                            w3-circle
                            w3-round-xlarge
                            w3-xxlarge
                            w3-padding-16
                            w3-margin-top"
                    href={config.url}
                >
                    <i className="fab fa-spotify" />
                    {" "}
                    Login with Spotify
                </a>
                <br />
                <div
                    className="w3-large w3-padding-16 w3-margin-top"
                    onClick={this.handleAnonymousClick}
                >
                    <u>
                        Or use without login
                    </u>
                </div>
            </div>
        );

    }

    handleCookieChange () {

        const {playing} = this.state;
        if (Options.getOptions().playing !== playing) {

            this.setState({"playing": Options.getOptions().playing});

        }

    }

    setCodeState (codeState) {

        this.setState({
            "codeToTokenSucessfull": codeState
        });

    }

    // eslint-disable-next-line max-lines-per-function
    render () {

        const selectedPlaylist = Query.getSelectedPlaylist(),
            token = Query.getToken(),
            {playing, codeToTokenSucessfull} = this.state;

        let backgroundStyles = {};
        if (selectedPlaylist) {

            backgroundStyles = {"backgroundImage": `url(${
                selectedPlaylist.images[0].url
            })`};

        }
        if (codeToTokenSucessfull) {

            this.resetCodeState();

            return <Redirect to="/" />;

        }

        return (
            <div className="App">
                {this.getHeader()}
                <div className="w3-content w3-display-container w3-center">
                    {token && !selectedPlaylist &&
                        <PlaylistSelector
                            rerenderAppCall={this.rerenderParentCallback}
                            rerenderParentCallback={this.rerenderParentCallback}
                        />}

                    {token && selectedPlaylist && !playing &&
                        <GameOptions
                            ref={this.gameOptionRef}
                            rerenderAppCall={this.rerenderParentCallback}
                        />}
                    {token && selectedPlaylist && playing &&
                        <Game
                            rerenderParentCallback={this.rerenderParentCallback}
                        />}
                    {!token &&
                        this.renderLoginButton()}
                </div>
                {selectedPlaylist &&
                    <div
                        className="background"
                        style={backgroundStyles}
                    />}
            </div>
        );

    }

}

export default App;

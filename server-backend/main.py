import sys
from flask import escape, jsonify, Response
import json
import Util
from Genius import Genius
from Playlist import PlaylistAPI
from Util import json_response, Status
import Spotify
import logging
import os

import firebase_admin
from firebase_admin import credentials, firestore

def get_songs(request):
    if request.method == 'OPTIONS':
        return ('', 204, Util.headers)

    if request.method != "GET":
        return json_response({"error": "invalid method"}, response_code=405)

    token = request.headers.get("Token")
    request_args = request.args

    db = Util.getDB()

    if not request_args or 'id' not in request_args:
        # From Databse?
        id = "37i9dQZF1DZ06evO2dYtbO"
    elif request_args and 'id' in request_args:
        id = request_args['id']

    mini = request_args and 'mini' in request_args
    
    playlist = PlaylistAPI(playlistID=id, spot=Spotify.getInstance(token, db=db))
    data = playlist.getSongs(mini, mini, mini)

    if playlist.spotify.status is not Util.Status.REQUEST_OK:
        return json_response({"error": str(playlist.spotify.status)}, playlist.spotify.status.value)

    return json_response(data)    

def analyse(request):
    if request.method == 'OPTIONS':
        return ('', 204, Util.headers)
    
    token = request.headers.get("Token")
    request_args = request.args

    db = Util.getDB()

    if not request_args or 'id' not in request_args:
        # From Databse?
        id = "37i9dQZF1DZ06evO2dYtbO"

    elif request_args and 'id' in request_args:
        id = request_args['id']

    playlist = PlaylistAPI(playlistID=id, spot=Spotify.getInstance(token, db=db))

    if request.method == "GET":
        # Analyse and get Songs
        playlist.getFeatures(True, True, True)
        data = playlist.analyse(Util.better_mean, roundStage=PlaylistAPI.ROUND_POST, roundAccuracy=3)

    elif request.method == "POST":
        # Playlist data should be given
        dictin = request.get_json()
        items = dictin["items"]
        
        data = playlist.analyse(Util.better_mean, roundStage=PlaylistAPI.ROUND_POST, roundAccuracy=3)

    return json_response(data)

def get_lyrics(request):
    if request.method == 'OPTIONS':
        return ('', 204, Util.headers)

    request_args = request.args
    if request.method == "GET":

        db = Util.getDB()

        if request_args and "track" in request_args and "artist" in request_args:
            track = request_args["track"]
            artist = request_args["artist"]
        else:
            return json_response({"error": "missing parameters"}, response_code=400)

        #print(f"Track {track} by {artist}")

        ## Scraping the Genius site is not compliant with it's TOS
        gen = Genius("GENIUS-API-TOKEN-HERE")
        url = gen.getSongURL(name=track, artist=artist, db=db)
        if url == None:
            if gen.status == Status.REQUEST_OK:
                return json_response({"error": str(Status.NOT_FOUND)}, Status.NOT_FOUND)

            return json_response({"error": str(gen.status)}, gen.status.value)
        #print(gen.status)
        lyr = gen.get_lyrics_from_url(url)
        if lyr == None:
            return json_response({"error": str(gen.status)}, gen.status.value)

        return json_response(lyr)
    else:
        return json_response({"error": "invalid method"}, response_code=405)


#https://accounts.spotify.com/authorize?client_id=59ad87ec5dfa4d2f9e6ff3d4fb11b147&redirect_uri=https://spottheverse.herobone.de&scope=user-top-read%20user-read-currently-playing%20user-read-playback-state%20playlist-read-collaborative%20playlist-read-private%20user-library-read&response_type=code&show_dialog=false

def auth(request):
    if request.method == 'OPTIONS':
        return ('', 204, Util.headers)

    request_args = request.args
    if request.method == "GET":

        redirectURI = "https://spottheverse.herobone.de"

        if "Origin" in request.headers:
            if "localhost" in request.headers.get("Origin"):
                redirectURI = "http://localhost:3000/"

        if request_args and "code" in request_args:
            spot = Spotify.getInstance(redirectURI=redirectURI)

            spot.getUserToken(request_args["code"])
            if spot.status != Status.REQUEST_OK:
                return json_response({"error": str(spot.status)}, spot.status.value)
            data = {
                "token": spot.token,
                "refresh": spot.refreshToken,
                "expires": spot.expires
            }
            return json_response(data)
        elif request_args and "refresh" in request_args:
            spot = Spotify.getInstance(redirectURI=redirectURI)

            spot.refreshToken = request_args["refresh"]

            spot.refreshUserToken()
            if spot.status != Status.REQUEST_OK:
                return json_response({"error": str(spot.status)}, spot.status.value)

            data = {
                "token": spot.token,
                "expires": spot.expires
            }
            return json_response(data)
        elif request_args and "pleaseiwantyourtoken" in request_args:
            spot = Spotify.getInstance(db=Util.getDB())
            
        else:
            return json_response({"error": "missing parameters"}, response_code=400)
    else:
        return json_response({"error": "invalid method"}, response_code=405)


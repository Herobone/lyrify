import json
from collections import namedtuple
import statistics
import requests
from enum import Enum
from flask import escape, jsonify, Response
from base64 import b64encode as _b64encode
import firebase_admin
from firebase_admin import credentials, firestore

def _json_object_hook(d):
        return namedtuple('X', d.keys())(*d.values())

def json2obj(data):
    return json.loads(data)

headers = {
    'Access-Control-Allow-Origin': '*',
    'Access-Control-Allow-Methods': '*',
    'Access-Control-Allow-Headers': '*',
    'Access-Control-Max-Age': '3600'
}

def list2json(data):
    def default(obj):
        if obj != None:
            return obj.__dict__
    
    return json.dumps(data,
            default=default,
            sort_keys=True,
            indent=None,
            separators=(',', ':'))

class Struct:
    def __init__(self, **entries):
        self.__dict__.update(entries)

class Serializeable:
    def toJSON(self):
        return json.dumps(self, default=lambda o: o.__dict__, 
            sort_keys=True, indent=None)

def getDB():
    try:
        cred = credentials.Certificate("./ServiceAccountKey.json")
    except:
        cred = credentials.ApplicationDefault()
    
    try:
        app = firebase_admin.initialize_app(cred)
    except:
        app = firebase_admin.get_app()
    db = firestore.client()
    return db

def json_response(data, response_code=200):
    return Response(list2json(data), status=response_code, mimetype="application/json", headers=headers)

class API:
    def __init__(self, baseUrl, token):
        self.baseUrl = baseUrl
        self.token = token

    def query(self, url, payload=None):
        headers = {"Authorization": f"Bearer {self.token}"}
        r = requests.get(
            f"{self.baseUrl}{url}", params=payload, headers=headers)

        if r.status_code == 200:
            self.status = Status.REQUEST_OK
            self.data =  r.json()
            return True

        elif r.status_code == 401:
            self.status = Status.TOKEN_INVALID
            self.data = None
            return False

        elif r.status_code == 404:
            self.status = Status.NOT_FOUND
            self.data = None
            return False

        elif r.status_code == 429:
            self.status = Status.THROTTLED
            self.data = None
            return False

        else:
            self.status = Status.OTHER_ERROR
            self.data = None
            return False

class Status(Enum):
    REQUEST_OK = 200
    BAD_REQUEST = 400
    TOKEN_INVALID = 401
    NOT_FOUND = 404
    DIV_NOT_FOUND = 404
    THROTTLED = 429
    OTHER_ERROR = 500

def b64encode(msg: str) -> str:
    """
    Base 64 encoding for Unicode strings.
    """
    return _b64encode(msg.encode()).decode()

def better_mean(data):
    try:
        return statistics.harmonic_mean(data)
    except:
        return statistics.mean(data)
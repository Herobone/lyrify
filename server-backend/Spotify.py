import requests
from Util import API, Status
from Util import b64encode
import time
import os

def getInstance(token=None, db=None, redirectURI=None):
    CLLIENT_ID = os.environ.get("CLLIENT_ID")
    CLIENT_SECRET = os.environ.get("CLIENT_SECRET")
    
    spot = SpotifyAPI(clientID=CLLIENT_ID, clientSecret=CLIENT_SECRET, token=token, redirectURI=redirectURI)

    if (token == None or token == "Anonymous") and db != None:
        spot.getClientToken(db)

    return spot

class SpotifyAPI(API):

    def __init__(self,
                clientID=None,
                clientSecret=None,
                redirectURI=None,
                token=None,
                refreshToken=None,
                expiresAT=time.time()):
        self.token = token
        self.refreshToken = refreshToken
        self.expires = 0

        self.clientID = clientID
        self.clientSecret = clientSecret
        self.redirectURI = redirectURI
        self.expiresAT = expiresAT

        super().__init__("https://api.spotify.com/v1/", self.token)

    def getUserToken(self, code):
        self.code = code
        auth = b64encode(f"{self.clientID}:{self.clientSecret}")

        headers = {'Authorization': f'Basic {auth}'}

        payload = {
            'code': code,
            'redirect_uri': self.redirectURI,
            'grant_type': 'authorization_code',
            #"client_id": self.clientID,
            #"client_secret": self.clientSecret
        }

        r = requests.post("https://accounts.spotify.com/api/token",
                    data=payload,
                    headers=headers)

        data = r.json()
        self.processResponseCode(r.status_code)
        if not "error" in data:
            self.token = data["access_token"]
            self.refreshToken = data["refresh_token"]
            self.expires = data["expires_in"]
        else:
            print(data)
        return self.token

    def processResponseCode(self, code):
        if code == 200:
            self.status = Status.REQUEST_OK
            return True

        elif code == 400:
            self.status = Status.BAD_REQUEST
            return False

        elif code == 401:
            self.status = Status.TOKEN_INVALID
            return False

        elif code == 404:
            self.status = Status.NOT_FOUND
            return False

        elif code == 429:
            self.status = Status.THROTTLED
            return False

        else:
            self.status = Status.OTHER_ERROR
            return False

    def refreshUserToken(self):
        auth = b64encode(f"{self.clientID}:{self.clientSecret}")

        headers = {'Authorization': f'Basic {auth}'}

        payload = {
            'refresh_token': self.refreshToken,
            'grant_type': 'refresh_token'
        }

        r = requests.post("https://accounts.spotify.com/api/token",
                    data=payload,
                    headers=headers)

        data = r.json()
        self.processResponseCode(r.status_code)
        if not "error" in data:
            self.token = data["access_token"]
            self.expires = data["expires_in"]
        return self.token

    def getClientToken(self, db):
        doc_ref = db.collection(u"serverData").document(u"token")
        doc = doc_ref.get()
        if doc.exists:
            data = doc.to_dict()
            self.token = data["token"]
            self.expiresAT = float(data["expires"])
            
        if self.expiresAT > (time.time() + 100):
            return self.token

        print("Getting new Client Token")
        
        auth = b64encode(f"{self.clientID}:{self.clientSecret}")

        headers = {'Authorization': f'Basic {auth}'}

        payload = {
            'grant_type': 'client_credentials'
        }

        r = requests.post("https://accounts.spotify.com/api/token",
                    data=payload,
                    headers=headers)

        data = r.json()
        self.processResponseCode(r.status_code)

        if not "error" in data:
            self.token = data["access_token"]
            self.expires = data["expires_in"]
            self.expiresAT = time.time() + self.expires

            doc_ref.set({
                u"expires": self.expiresAT,
                u"token":   self.token
            })
        else:
            self.token = None
        return self.token




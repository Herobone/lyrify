from bs4 import BeautifulSoup
import re
import requests
from Util import API, Status
import time

class Genius(API):

    def __init__(self, token):
        super().__init__("https://api.genius.com/", token)

    def search(self, search):
        return self.query("search", payload={"q": search})

    def getSongURL(self, name, artist, db=None):
        track = name.split("-")[0]
        track = re.sub(r'(\(.*?\))*', '', track)
        track = re.sub(r'(\[.*?\])*', '', track).strip()
        searchQuery = f"{track} {artist}"
        doc_ref = None
        url = None
        if db != None:
            doc_ref = db.collection(u"lyricsCache").document(searchQuery)
            doc = doc_ref.get()
            if doc.exists:
                data = doc.to_dict()
                url = data["url"]
                self.status = Status.REQUEST_OK
                return url
            
        if not self.search(searchQuery):
            print(self.status)
            return None
        for hit in self.data["response"]["hits"]:
            res = hit["result"]
            if res["primary_artist"]["name"] == artist:
                url = res["path"]
                break

        if url == None:
            self.status = Status.NOT_FOUND

        doc_ref.set({
                u"url":     url
            })
        return url

    def get_lyrics_from_url(self, url):
        newUrl = f"https://genius.com{url}"
        #print("Url:", url, "New Url:", newUrl)
        page = requests.get(newUrl)

        if page.status_code == 404:
            print(f"Site {newUrl} not found")
            self.status = Status.NOT_FOUND
            return None

        # Scrape the song lyrics from the HTML
        html = BeautifulSoup(page.text, "html.parser")
        div = html.find("div", class_="lyrics")
        if not div:
            self.status = Status.DIV_NOT_FOUND
            return None # Sometimes the lyrics section isn't found

        # Scrape lyrics if proper section was found on page
        lyrics = div.get_text()
        lyrics = re.sub(r'(\[.*?\])*', '', lyrics) # [Chorus], [Verse] and so on removed
        lyrics = re.sub(r'\n{2}', '', lyrics)  # Gaps between verses
        return lyrics.strip("\n").split("\n")